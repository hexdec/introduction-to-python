def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV

def GetBin(inDigit):
    if inDigit<0:
        dig=inDigit*-1
    else:    
        dig=inDigit
    binDig=""
    out=""
    while dig>1:
        binDig+=str(dig%2)
        dig=int(dig/2)
        
    if dig==1 or dig==0: binDig+=str(dig)
    for i in range(1,len(binDig)+1):
        out=out+binDig[len(binDig)-i]

    if inDigit<0: out=str(int(out)*-1)
    return out

inDigit=inputDigit("Введите число: ")
#print(bin(inDigit))   # 😁
print("Двоичное:",GetBin(inDigit))