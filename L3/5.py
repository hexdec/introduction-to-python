def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV

def Fibo(n):
    if n==0: return 0
    if n==1: return 1
    if (n < 0): 
        return Fibo(n*(-1))*((-1)**((n*(-1))+1))
    return Fibo(n-1)+Fibo(n-2)


inDig=inputDigit("Введите n: ")
negafiboList=[]
for i in range(-inDig,inDig+1):
    negafiboList.append(Fibo(i))

print(negafiboList)