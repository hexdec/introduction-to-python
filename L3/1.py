import random

def ListGenerate(length,min=1,max=10):
    return [random.randint(min,max) for i in range(0,length)]


genList=ListGenerate(10)
elSum=0
for i in range(0,len(genList)):
    if (i % 2 > 0):
        elSum+=genList[i]
print("Сгенерированный список:",genList)
print("Сумма нечётных элементов",elSum)