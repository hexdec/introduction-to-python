import random
def ListGenerateReal(length,min=1,max=10):
    return [random.randint(min,max)+round(random.random(),2) for i in range(0,length)]


def MinMaxFractSub(inList):
    inList=[round((i%1),2) for i in inList]
    print(inList)
    min=inList[0]
    max=inList[0]
    for i in range(1,len(inList)):
        if inList[i] > max: max=inList[i]
        if inList[i] < min: min=inList[i]
    print(f"Max: {max}, Min: {min}")
    return (max-min)

genList=ListGenerateReal(random.randint(5,11))
print("Сгенерированный список:",genList)
print("min-max:",MinMaxFractSub(genList))
