def inputFloatDigit(prompt=""):
    try:
        inV=float(input(prompt))
    except:
        return inputFloatDigit("Вы ввели не число. Введите число: ")
    else:
        return inV



x=inputFloatDigit("Введите ккординату точки X (X <> 0): ")
y=inputFloatDigit("Введите ккординату точки Y (Y <> 0): ")

if (x == 0 or y == 0): print("Неизвестно.  X,Y <> 0")
if (x > 0 and y > 0): print("1")
if (x < 0 and y > 0): print("2")
if (x < 0 and y < 0): print("3")
if (x > 0 and y < 0): print("4")
