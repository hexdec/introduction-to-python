def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV

x=inputDigit("Введите номер четверти: ")

if (x==1): print('X in range (0;∞), Y in range (0;∞)')
if (x==2): print('X in range (-∞;0), Y in range (0;∞)')
if (x==3): print('X in range (-∞;0), Y in range (-∞;0)')
if (x==4): print('X in range (0;∞), Y in range (-∞;0)')
