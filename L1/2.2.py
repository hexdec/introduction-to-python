from cmath import sqrt


def inputListDigit(prompt=""):
    try:
        inV=list(map(int,input(prompt).split(",")))
    except:
        return inputListDigit("Вы ввели не число. Введите число: ")
    else:
        return inV

def GetLineLength(p1,p2):
    return ((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)**0.5

A=inputListDigit("Введите координаты точки A (x,y) через запятую: ")
B=inputListDigit("Введите координаты точки B (x,y) через запятую: ")

print(GetLineLength(A,B))
print(f"Расстояние между точками A{A} и B{B}:",round(GetLineLength(A,B),2))