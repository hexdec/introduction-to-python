print("Введите номер дня недели(от 1 до 7)")

def inputDigit():
    try:
        inV=int(input())
    except:
        print("Вы ввели не число. Введите число")
        return inputDigit()
    else:
        return inV

def CheckDays(input):
    if (input>=1 and input <= 7):
        return input
    else:
        print('Введите номер дня недели(от 1 до 7)')
        return CheckDays(inputDigit())
        

def IsWeekend(inDay):
    if (inDay == 6 or inDay == 7):
        print ('Да')
    else:
        print ('Нет')

#IsWeekend(CheckDays(inputDigit()))  #Сложно сказать, как более читаемо )
day=CheckDays(inputDigit())
IsWeekend(day)