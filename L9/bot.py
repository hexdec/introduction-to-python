from telegram import Update, KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove, Update, WebAppInfo
from telegram.ext import ApplicationBuilder, CommandHandler, ContextTypes,MessageHandler


async def hello(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await update.message.reply_text(f'Hello {update.effective_user.first_name}')

async def step(update: Update, context: ContextTypes.DEFAULT_TYPE):
    reply_keyboard = [["1", "2", "3"],["4", "5", "6"],["7", "8", "9"]]

    await update.message.reply_text(
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder="необходимо выбрать куда ставим X или O"
        ),
    )

async def printText(update: Update, context: ContextTypes.DEFAULT_TYPE,text):

    await update.message.reply_text(text)





def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV


def DisplayArea(inDict):
    print(f"|{inDict['x1']}|{inDict['x2']}|{inDict['x3']}|")
    print(f"|{inDict['x4']}|{inDict['x5']}|{inDict['x6']}|")
    print(f"|{inDict['x7']}|{inDict['x8']}|{inDict['x9']}|") 

def WinnerCheck(inArea):
    for i in range(1,4):
        if (inArea[f"x{i}"]==inArea[f"x{i+3}"]==inArea[f"x{i+6}"]!="_"):
            return True
        if (inArea[f"x{1+3*(i-1)}"]==inArea[f"x{2+3*(i-1)}"]==inArea[f"x{3+3*(i-1)}"]!="_"):
            return True
    if (inArea[f"x1"]==inArea[f"x5"]==inArea[f"x9"]!="_"):
        return True
    if (inArea[f"x3"]==inArea[f"x5"]==inArea[f"x7"]!="_"):
        return True
    return False

async def Game(update: Update, context: ContextTypes.DEFAULT_TYPE):

    li={"x1":"_","x2":"_","x3":"_","x4":"_","x5":"_","x6":"_","x7":"_","x8":"_","x9":"_"}

    gamers=["x","o"]
    await update.message.reply_text("|1|2|3|\n|4|5|6|\n|7|8|9|")

    winner=False
    history=[]
    while winner==False and len(history)<9:
        for gamer in gamers:
            selector=-1
            while selector not in range(1,10) or selector in history:
                #selector=inputDigit(f"Ходит {gamer}. Введите номер поля: ")
                app.add_handler(MessageHandler("~filters.UpdateType.*", f"Ходит {gamer}. Введите номер поля: "))
                update.message.reply_text(f"Ходит {gamer}. Введите номер поля: ")
            history.append(selector)
            li[f"x{selector}"]=gamer
            winner=WinnerCheck(li)
            DisplayArea(li)
            if (winner==True): 
                print(f"Победил {gamer}")
                break
            if (winner==False and len(history)>=9): 
                print(f"Ничья")
                break


app = ApplicationBuilder().token("5372732673:AAF5rGFWGKbblf6UTc0mJmPUe7UW2TKbJ1s").build()

app.add_handler(CommandHandler("hello", hello))
app.add_handler(CommandHandler("start", Game))
print(app.bot_data)
app.run_polling()

