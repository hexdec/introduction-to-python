import random

def GetListUniq(inList):
    outList=[]
    for i in inList:
        if i not in outList: outList.append(i)
    return outList



def ListGenerate(length,min=1,max=10):
    return [random.randint(min,max) for i in range(0,length)]



newList=ListGenerate(15)
print("Сгенерированный список:",newList)
print("Cписок уникальных элементов:",GetListUniq(newList))
