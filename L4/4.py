import random,uuid
workFile=f"./polynoms_{uuid.uuid4()}.txt"
def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
        if inV==0: return inputDigit("Введите число > 0: ")
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV

def WriteToFile(data,filePath):
    with open (filePath, 'w') as file:
        file.writelines(data)

def PolyGen(inDig,outfilePath=""):
    resultString=""
    const=True
    for i in range(inDig,-1,-1):
        multiplier=random.randint(-10,10)
        if multiplier > 0 and resultString !="" : sign="+"
        else: sign=""
        if multiplier != 0:
            if i > 0: const=False
            if i > 1:
                resultString+=f"{sign}{multiplier}*x^{i}"
            if i == 1:
                resultString+=f"{sign}{multiplier}*x"
            if i == 0:
                resultString+=f"{sign}{multiplier}"

    if const==False: resultString+="=0"
    else: resultString+=f"={resultString}"
    if outfilePath != "": WriteToFile(resultString,outfilePath)
    return resultString


k=inputDigit("Введите k: ")

print(PolyGen(k,workFile))