import re
from socket import TCP_FASTOPEN

#file1=input("Введите путь файла 1: ")
#file2=input("Введите путь файла 2: ")

file1="C:\\Users\\Sneg\\GB\\ProgII\\polynoms_8fbe4712-2f34-4147-a10a-39fc881e2bd5.txt"
file2="C:\\Users\\Sneg\\GB\\ProgII\\polynoms_84363e53-b0ad-4369-9e05-d71247fcf7fa.txt"

def PolyDict(inPol):
    xs=re.findall("x\^\d*|x",inPol)
    cinPol=inPol
    xss={}
    for i in xs:
        tmp=i.replace('^','\^')
        #print(f"-?\d*\*{tmp}")
        fStr=(re.findall(f"-?\d*\*{tmp}[^\^]",inPol))
        cinPol=cinPol.replace(fStr[0][:-1],"")
        #print(fStr,cinPol)
        xss[i]=int(fStr[0].split(f"*{i}")[0])
    c=(re.findall("[+|-]\d*",cinPol))
    xss["c"]=(int(c[0]))
    return xss

def SumPol(pol1,pol2):
    pol1=PolyDict(pol1)
    pol2=PolyDict(pol2)
    outPolD={}
    if (len(pol1) >= len(pol2)):
        op1=pol1
        op2=pol2
    else:
        op1=pol2
        op2=pol1
    for i in op1:
        if i in op2:
            outPolD[i]=op1[i]+op2[i]
        else:
            outPolD[i]=op1[i]
    for i in op2:
        if i not in op1:
            outPolD[i]=op2[i]
    outPol=""
    for i in outPolD:
        if i!="c":
            if outPolD[i] > 0:
                tmp=f"+{outPolD[i]}"
            else:
                tmp=outPolD[i]
            outPol+=f"{tmp}*{i}"
    if "c" in outPolD:
        if outPolD['c'] > 0:
            tmp=f"+{outPolD['c']}"
        else:
            tmp=outPolD['c']
        outPol+=f"{tmp}"
    outPol+="=0"
    return(outPol)
    


with open (file1) as file:
    poly1=file.readline()
with open (file2) as file:
    poly2=file.readline()


print("Полином 1:",poly1)
print("Полином 2:",poly2)

print("Сумма полиномов:",SumPol(poly1,poly2))