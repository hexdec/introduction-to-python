from cmath import pi


def inputRealDigit(prompt=""):
    try:
        inV=float(input(prompt))
        if inV==0: return inputRealDigit("Введите число > 0: ")
    except:
        return inputRealDigit("Вы ввели не число. Введите число: ")
    else:
        return inV


inDig=inputRealDigit("Введите точность: ")

sig=int(1/inDig)

precision=0
while sig > 1:
    precision+=1
    sig=(sig/10)

print(round(pi,precision))