
def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
        if inV==0: return inputDigit("Введите число > 0: ")
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV

def GetSimpleDig(dig):
    if (dig == 1 or dig == 2 or dig == 3): return dig
    for i in range(2,int(dig/2)+15):
        if dig%i==0:
            return GetSimpleDig(int(dig/i))
        elif (i>=int(dig/2)) : return dig


def GetSimpleList(dig):
    multi=1
    wdig=dig
    outList=[]
    while (multi < dig):
        tmp=GetSimpleDig(wdig)
        outList.append(tmp)
        wdig=int(wdig/tmp)
        multi=1
        for i in outList:
            multi*=i
    return outList

inDig=inputDigit("Введите N: ")
print("Разложение на простые делители:",GetSimpleList(inDig))

