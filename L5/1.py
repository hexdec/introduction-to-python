
import random
from time import sleep

maxSweetinStep=28
Sweets=2021

def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV

def Bot(points):
    if points - maxSweetinStep<=0: return points
    if points - maxSweetinStep > maxSweetinStep*5: return maxSweetinStep
    return  points - (maxSweetinStep+1)*int((points/(maxSweetinStep+1)))

def Menu():
    print("Выберите вариант игры:")
    print("1. Человек против человека.")
    print("2. Человек против бота.")
    selector=input()
    if selector=="1": hGame(1,Sweets)
    if selector=="2": hGame(2,Sweets)

def hGame(mode,points):
    print("Жеребьёвка:")
    gamer1=0
    gamer2=0
    nameGamer1="Игрок 1"
    if mode==1:
        nameGamer2="Игрок 2"
    else:
        nameGamer2="Супер Пупер Мега Бот"
    print("Всего конфет на столе:", points)
    gamers=[nameGamer1,nameGamer2]
    while gamer1 == gamer2:
        gamer1=random.randint(1,6)
        sleep(1)
        gamer2=random.randint(1,6)
        print("Игрок 1:",gamer1)
        print("Игрок 2:",gamer2)
    if gamer1>gamer2:
        step=gamers[0]
        s=1
    else:
        step=gamers[1]
        s=0
    print("Игроки выбирают количество конфет, которые хотят забрать со стола:" )
    print(f"Первым ходит игрок {step}")
    while points>0:
        if s==1: s=0
        else: s=1
        step=gamers[s]
        sel=-1
        while sel > maxSweetinStep or sel < 1 or (points-sel) < 0:
            if mode == 1 or s==0:
                sel=inputDigit(f"Ход игрока {step}: ")
            else:
                sel=Bot(points)
                print(f"Ход игрока {step}: ",sel)
            if (points-sel) < 0: print("Ну как можно забрать со стола больше конфет, чем там есть🤪")
        points-=sel
        print("На столе осталось:",points,"конфет")
        
    print(f"Игрок {step}, поздравляем, вы победили!!!")


def bGame():
    pass


Menu()