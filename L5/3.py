file='C:\\Users\\Sneg\\GB\\ProgII\\zipin.txt'

def Zipper(filepath):
    with open (filepath,'r') as file:
        lines=file.read()

    #print(len(lines))
    zipLines=lines
    for i in range(0,len(zipLines)):
        if i<len(zipLines)-1:
            if zipLines[i+1]==zipLines[i]:
                k=1
                while zipLines[i+k]==zipLines[i] and i+k<len(zipLines)-1:
                    k+=1
                if k>2:
                    zipLines=zipLines[:i+1]+f"*{k}|"+zipLines[(i+k):]
    with open (f"{filepath}.arc",'w') as file:
        file.write(zipLines)


def Unzipper(filepath):
    unList=[]
    with open (filepath,'r') as file:
        lines=file.read()
    zipLines=lines
    for i in range(0,len(lines)):
        if lines[i]=="*":
            k=1
            while lines[i+k]!="|" and i+k<len(lines):
                k+=1
            unList.append(lines[i-1:i+k+1])

    for rle in unList:
        zipLines=zipLines.replace(rle,rle[0]*int(rle[2:].replace("|","")))
    with open (f"{filepath.replace('.arc','')}_decompress.txt",'w') as file:
        file.write(zipLines)
            
Zipper(file)
Unzipper(file+'.arc')