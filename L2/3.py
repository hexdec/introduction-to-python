def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV

def ListGenerate(length,display=False):
    result=[]
    result=[((1+ (1/i))**i) for i in range(1,length+1)]
    if (display==True):
        print("Сгенерирована последовательность: ",result)
    return result


def SummList(inList):
    result=0
    for i in range(0,len(inList)):
        result=result+inList[i]
    return result


n=inputDigit("Введите n (целое число): ")
print("Сумма элементов последовательности: ", round(SummList(ListGenerate(n,True)),2))