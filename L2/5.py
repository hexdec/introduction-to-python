from time import time_ns
import uuid

def GetRandom(min,max):   #сделано только для положительных минимумов и максимумов. Отриц. и не требуются, по идее, в текущей задаче 😃
    ranges=[i for i in range(min,max+1)]
    idstr=f"{uuid.uuid4()}".replace("-","")
    idstrint=str((idstr,16))
    h=(hash(idstr))
    if (min >= 0) and (h < 0): h=h*(-1)
    h=(hash(str(h+time_ns())))
    if (min >= 0) and (h < 0): h=h*(-1)

    sum=0
    for i in range(0,len(str(h))):
        sum=sum+int(str(h)[i])

    if (sum > len(ranges)): 
        sum=sum % len(ranges)
    else:
        sum=sum**2
        sum=sum % len(ranges)

    return ranges[sum]

def ListShuffle(inList):
    outList=[]
    tempList=[-1]
    temp=-1
    for i in range(0,len(inList)):
        while temp in tempList:
            temp=GetRandom(0,len(inList)-1)
        tempList.append(temp)
        outList.append(inList[temp])
    return outList

list=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
print("Входной список:",list)
print("Перемешанный список:",ListShuffle(list))

