#filepath=e:\\positionFil

from msilib.schema import IniLocator


def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV

def ListGenerate(length,display=False):
    result=[]
    result=[i for i in range(-length,length+1)]
    if (display==True):
        print("Сгенерирована последовательность: ",result)
    return result


def SummList(inList):
    result=0
    for i in range(0,len(inList)):
        result=result+inList[i]
    return result

def GetMultiRead(inList,filePath):
    with open (filePath) as file:
        lines = file.readlines()
        lines = [int(line.rstrip()) for line in lines]
    print("Позиции из файла:",lines)
    result=inList[lines[0]]
    lines.pop(0)
    for i in (lines):
        result=result*inList[i]
    return result

n=inputDigit("Введите n (целое число): ")

genList=ListGenerate(n,True)
multi=GetMultiRead(genList,".\\introduction-to-python\\L2\\positionFile.txt")

print(f"Произведение элементов: {multi}")