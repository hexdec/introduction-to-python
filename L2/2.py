def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV
def GetFactList(length):
    out=[]
    for i in range(1,length+1):
        temp=1
        for k in range(1,i+1):
            temp=temp*k
        out.append(temp)
    return out

n=inputDigit("Введите N (целое число): ")
print(GetFactList(n))