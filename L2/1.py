def inputDigit(prompt=""):
    try:
        inV=int(input(prompt))
    except:
        return inputDigit("Вы ввели не число. Введите число: ")
    else:
        return inV

n=inputDigit("Введите n (целое число): ")
b={}
for i in range(1,n+1):
    b[i]=3*i+1
print(b)